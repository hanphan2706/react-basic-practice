import React, { Component } from 'react';
import './App.css';

import ListNotes from './components/list-notes/list-notes.component';
import TypeMessage from './components/type-message/type-message.component';

export default class App extends Component {
  // WHY WE CAN NOT SET LIKE THAT
  // state = {
  //   currentNote: {},
  //   step: 1,
  // }

  constructor(props) {
    console.log('App_constructor');
    super(props);
    console.log(this.props);
    
    // getInitialState()
    // It is important to keep in mind that if we do not define a state in the constructor/getInitialState
    // then the state will be undefined.
    // Because the state is undefined and not an empty Object ({}),
    // if you try to query the state later on this will be an issue.

    this.state = {
      currentNote: {},
      step: 1,
    }

    // USE ARROW FUNC INSTEAD
    // this.handleNoteChange = this.handleNoteChange.bind(this);
  }

  handleNoteChange = (currentNote) => {
    this.setState({
      currentNote: currentNote,
      step: 2
    }) 
  }

  handleBack = (step, e) => {
    this.setState({
      step: step
    }) 
  }

  // USE '&&' TO SHOW/HIDE INSTEAD OF STYLE DISPLAY
  // <div className="demo-step-one"
  //    style={ this.state.step === 1 ? {display: 'block'} : {display: 'none'}}> 
  //  <ListNotes onNoteChange={this.handleNoteChange} />
  // </div>

  render() {
    console.log('App_render');
    return (
      <div className="App">
        { this.state.step === 1 &&
          <div className="demo-step-one"> 
            <ListNotes onNoteChange={this.handleNoteChange} />
          </div>
        }
        { this.state.step === 2 &&
          <div className="demo-step-two">
            <TypeMessage currentNote={this.state.currentNote} />
          </div>
        }
        <div className="demo-btn-control"
             style={ this.state.step === 2 ? {display: 'block'} : {display: 'none'}}>
          <button onClick={(e) => this.handleBack(this.state.step - 1, e)}>Back</button>
        </div>
      </div>
    );
  }

  componentWillMount() {
    console.log('App_componentWillMount');
  }
  
  componentDidMount() {
    console.log('App_componentDidMount');
  }

  componentWillReceiveProps() {
    console.log('App_componentWillReceiveProps');
  }

  shouldComponentUpdate() {
    console.log('App_shouldComponentUpdate');
    return true;
  }

  componentWillUpdate() {
    console.log('App_componentWillUpdate');
  }

  componentDidUpdate() {
    console.log('App_componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('App_componentWillUnmount');
  }
}