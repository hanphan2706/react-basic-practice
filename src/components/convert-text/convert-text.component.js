import React, { Component } from 'react';
import PropTypes from 'prop-types';

// DIFFERENT BETWEEN EXPORT AND EXPORT DEFAULT
// export default class ConvertText extends Component {
export class ConvertText extends Component {
  // WE MAY NOT SUPER PROPS HERE BECAUSE ON RENDER FUNCTION IT WILL HAVE PROPS
  // IF WE DEFINED CONSTRUCTOR, WE MUST HAVE SUPER PROPS, IF NOT, PROPS IS UNDEFINED
  // constructor(props) {
  //   super(props);
  // }

  render() {
    let textDom = '';
    let regex = '';

    this.props.listUsersHighlight.map( item => regex += '(' + item + ')|');
    regex += '(\n)';
    regex = new RegExp(regex);
    
    let parts = this.props.text.split(regex);
    if (parts) {
      textDom = parts.map((item, index) => {
        if (item) {
          if (item === '\n') {
            return <br key={'lineBreak_' + index} />;
          }
          const indexOfKeyword = this.props.listUsersHighlight.indexOf(item);
          if ( indexOfKeyword >= 0) {
            return <strong key={'hightlight_' + index}>{item}</strong>;
          } else {
            return <span key={'normalText_' + index}>{item}</span>;
          }
        }
        return true; // ERROR: Expected to return a value at the end of arrow function array-callback-return
      })
    }

    return <div>{textDom}</div>;
  }
}

ConvertText.defaultProps = {
  text: '',
  listUsersHighlight: [],
};

ConvertText.propTypes = {
  text: PropTypes.string.isRequired,
  listUsersHighlight: PropTypes.array
}