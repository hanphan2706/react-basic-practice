import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './type-message.component.css';
import { ConvertText } from '../convert-text/convert-text.component';
import CustomDropdown from '../custom-dropdown/custom-dropdown.component';

export default class TypeMessage extends Component {
  // DEFINED ON THE TOP TO LET OTHER PEOPLE KNOW THAT YOU HAVE COMPOSE SOMETHING DOM
  listColorsSupportDOM = [];

  constructor(props) {
    console.log('TypeMessage_constructor');
    super(props);

    this.listColorsSupport = [
      { name: 'black', value: '#000000'},
      { name: 'white', value: '#ffffff'},
      { name: 'green', value: '#20a700'},
      { name: 'orang', value: '#e8910a'},
      { name: 'purple', value: '#bd04a5'}
    ];
    this.listUsers = [
      { id: 1, name: 'Person_1', ortherName: 'ABC_1'},
      { id: 2, name: 'Person_2', ortherName: 'ABC_2'},
      { id: 3, name: 'Person_3', ortherName: 'ABC_3'},
      { id: 4, name: 'Person_4', ortherName: 'ABC_4'},
      { id: 5, name: 'Person_5', ortherName: 'ABC_5'}
    ]

    this.textareaInput = React.createRef();

    this.state = {
      message: '',
      textColor: '',
      listUsersHighlight: []
    }

    // this.state = {
    //   message: '',
    //   textColor: '',
    //   listUsersHighlight: [],
    //   valueToShow: 'name'
    // }
    this.composeListColorsDOM();
  }

  typeMessageHandle = (e) => {
    this.setState({
      message: e.target.value
    })
  }

  changeColorHandle = (e) => {
    this.setState({
      textColor: e.target.value
    })
  }

  dragEndHandle = (value) => {
    let listUsersHighlight = this.state.listUsersHighlight;
    if (listUsersHighlight.indexOf(value) < 0 ){
      listUsersHighlight.push(value);
    }
    this.setState((prevState, props) => ({
      message: prevState.message + value,
      listUsersHighlight: listUsersHighlight
    }));
    this.textareaInput.current.focus();

    // this.setState((prevState, props) => ({
    //   message: prevState.message + value,
    //   listUsersHighlight: listUsersHighlight,
    //   valueToShow: 'ortherName'
    // }));

  }

  composeListColorsDOM() {
    this.listColorsSupportDOM = this.listColorsSupport.map((item, index) => {
      return <option value={item.value} key={'color_' + index} >{item.name}</option>;
    })
  }

  // <CustomDropdown list={this.listUsers}
  //                 valueToShow={this.state.valueToShow}
  //                 onDragEnd={this.dragEndHandle} />

  render() {
    console.log('TypeMessage_render');
    if (this.props.currentNote) {
      return (
        <div className="demo-step-two-container">
          <div className="demo-step-two--left demo-step-two--right">
            <div className="options-container">
              <select style={{marginRight: '2rem'}}
                      onChange={this.changeColorHandle}>
                      {this.listColorsSupportDOM}
              </select>
              <CustomDropdown list={this.listUsers}
                              valueToShow={ undefined }
                              onDragEnd={this.dragEndHandle} />
            </div>
            <div className="type-message-container">
              <textarea rows="10"
                        ref={this.textareaInput}
                        value={this.state.message}
                        onChange={this.typeMessageHandle} />
            </div>
          </div>
          <div className="demo-step-two--right">
            <div className="note-container"
                 style={{background: this.props.currentNote.color,
                         color: this.state.textColor}}>
              <ConvertText text={this.state.message}
                           listUsersHighlight={this.state.listUsersHighlight} />
            </div>
          </div>
        </div>
      );
    } else {
      return <div>Something is wrong</div>;
    }
  }

  componentWillMount() {
    console.log('TypeMessage_componentWillMount');
  }
  
  componentDidMount() {
    console.log('TypeMessage_componentDidMount');
  }

  componentWillReceiveProps() {
    console.log('TypeMessage_componentWillReceiveProps');
  }

  shouldComponentUpdate() {
    console.log('TypeMessage_shouldComponentUpdate');
    return true;
  }

  componentWillUpdate() {
    console.log('TypeMessage_componentWillUpdate');
  }

  componentDidUpdate() {
    console.log('TypeMessage_componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('TypeMessage_componentWillUnmount');
  }
}

TypeMessage.propTypes = {
  currentNote: PropTypes.object.isRequired,
}