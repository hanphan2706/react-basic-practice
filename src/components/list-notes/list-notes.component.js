import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './list-notes.component.css';

// DIFFERENT BETWEEN EXPORT AND EXPORT DEFAULT
export default class ListNotes extends Component {
  constructor(props) {
    console.log('ListNotes_constructor');
    super();
    console.log(this.props);
    
    this.listNotes = [
      {name: 'KEEP', color: '#5782ff'},
      {name: 'STOP', color: '#ff765f'},
      {name: 'DISCUSS', color: '#fff249'}
    ]
    this.state = {
      currentNote: this.listNotes[0]
    }
  }

  handleChangeNote = () => {
    let index = this.listNotes.indexOf(this.state.currentNote);
    index = index === this.listNotes.length - 1 ? 0 : index + 1;
    this.setState({
      currentNote: this.listNotes[index]
    })
  }

  handleChooseNote = (e) => {
    if (typeof(this.props.onNoteChange) === 'function') {
     this.props.onNoteChange(this.state.currentNote);
    }
  }

  render() {
    console.log('ListNotes_render');
    return (
      <div className="demo-step-one-container">
        <div className="demo-arrow"
             onClick={this.handleChangeNote}>
             &larr;
        </div>
        <div className="note-container">
          <div style={{background: this.state.currentNote.color}}
               onClick={this.handleChooseNote}
               className="note-context">
             <h1>{this.state.currentNote.name}</h1>
          </div>
        </div>
        <div className="demo-arrow"
             onClick={this.handleChangeNote}>
             &rarr;
        </div>
      </div>
    );
  }

  componentWillMount() {
    console.log('ListNotes_componentWillMount');
  }
  
  componentDidMount() {
    console.log('ListNotes_componentDidMount');
  }

  componentWillReceiveProps() {
    console.log('ListNotes_componentWillReceiveProps');
  }

  shouldComponentUpdate() {
    console.log('ListNotes_shouldComponentUpdate');
    return true;
  }

  componentWillUpdate() {
    console.log('ListNotes_componentWillUpdate');
  }

  componentDidUpdate() {
    console.log('ListNotes_componentDidUpdate');
  }

  componentWillUnmount() {
    // IT WILL HAPPEND AFTER NEXT STEP COMPONENTS RENDER
    // SO YOU CAN NOT CHANGE SOMETHING WILL INFECTTING NEXT STEP COMPONENTS
    // YOU ONLY CAN DO IT ON PARENT COMPONENT OF NEXT STEP COMPONENTS
    console.log('ListNotes_componentWillUnmount');
  }
}

ListNotes.propTypes = {
  onNoteChange: PropTypes.func,
}