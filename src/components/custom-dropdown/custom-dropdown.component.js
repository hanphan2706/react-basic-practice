import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './custom-dropdown.component.css';

export default class CustomDropdown extends Component {
  constructor(props) {
    console.log('CustomDropdown_constructor');
    super(props);

    this.state = {
      isShowList: false
    }

    this.listDom = [];
    this.compostListDom();
  }

  dragEndHandle = (e) => {
    if (typeof(this.props.onDragEnd) === 'function') {
      this.props.onDragEnd(e.target.dataset.value);
    }
    this.setState({
      isShowList: false
    })
  }

  chooseFromListHandle = () => {
    // Because this.props and this.state may be updated asynchronously,
    // you should not rely on their values for calculating the next state.
    this.setState((prevState, props) => ({
      isShowList: !prevState.isShowList
    }));
  }

  compostListDom() {
    this.listDom = this.props.list.map((item, index) => {
      return <li key={'dropdown_item_' + index}
                 data-value={item[this.props.valueToBind]}
                 draggable="true"
                 onDragEnd={this.dragEndHandle}>
                 {item[this.props.valueToShow]}</li>
    })
  }

  render() {
    console.log('CustomDropdown_render');
    // const listDom = this.props.list.map((item, index) => {
    //   return <li key={'dropdown_item_' + index}
    //              data-value={item[this.props.valueToBind]}
    //              draggable="true"
    //              onDragEnd={this.dragEndHandle}>
    //              {item[this.props.valueToShow]}</li>
    // })
    return (
      <div className="demo-custome-dropdown-container">
        <p onClick={this.chooseFromListHandle}>
          Choose from list...
          <i className="arrow down"></i>
        </p>
        <ul style={ this.state.isShowList ? {display: 'block'} : {display: 'none'}}>
          {this.listDom}
        </ul>
      </div>
    )
  }

  componentWillMount() {
    console.log('CustomDropdown_componentWillMount');
  }
  
  componentDidMount() {
    console.log('CustomDropdown_componentDidMount');
  }

  componentWillReceiveProps(nextProps) {
    // PROP HAS NOT CHANGED HERE
    // console.log('CustomDropdown_componentWillReceiveProps', nextProps, this.props);
    // this.listDom = [];
    // this.compostListDom();
  }

  shouldComponentUpdate(nextProps, nextStates) {
    console.log('CustomDropdown_shouldComponentUpdate');
    return true;
  }

  componentWillUpdate(nextProps) {
    // PROP HAS CHANGE HERE
    // console.log('CustomDropdown_componentWillUpdate',  nextProps, this.props);
    this.listDom = [];
    this.compostListDom();
  }

  componentDidUpdate() {
    console.log('CustomDropdown_componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('CustomDropdown_componentWillUnmount');
  }
}

// getDefaultProps()
CustomDropdown.defaultProps = {
  list: [], 
  valueToBind: 'name',
  valueToShow: 'name'
};

CustomDropdown.propTypes = {
  list: PropTypes.array.isRequired,
  valueToBind: PropTypes.string,
  valueToShow: PropTypes.string,
  onDragEnd: PropTypes.func,
}